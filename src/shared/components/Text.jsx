import React from "react";
import styled from "styled-components/native";

const Title = styled.Text`
  font-size: 15px;
  color: ${(props) => props.color};
`;

const Naming = styled.Text`
  color: ${(props) => props.theme.header};
  font-size: 13px;
  font-weight: bold;
`;

const Date = styled.Text`
  font-size: 11px;
  color: ${(props) => props.theme.date}
  font-weight: bold;
`;

const NameText = styled.Text`
  font-size: 30px;
  color:${(props) => props.theme.name};
  font-weight: bold;
`;

const Describe = styled.Text`
  color: ${(props) => props.theme.describe}
  font-size: 15px;
  font-weight: bold;
`;

const Description = styled.Text`
  color: ${(props) => props.color||props.theme.description}
  font-size: 17px;
  font-weight: bold;
`;

const RenderTextComponent = ({ type, text, color, theme }) => {
  return type == "title" ? (
    <Title color={color} theme={theme}>{text}</Title>
  ) : type == "naming" ? (
    <Naming theme={theme}>{text}</Naming>
  ) : type == "date" ? (
    <Date theme={theme}>{text}</Date>
  ) : type == "nameText" ? (
    <NameText theme={theme}>{text}</NameText>
  ) : type == "describe" ? (
    <Describe theme={theme}>{text}</Describe>
  ) : type == "description" ? (
    <Description color={color} theme={theme}>{text}</Description>
  ) : null;
};

export default function TextComponent({ type, text, color, theme, ...rest }) {
  return <RenderTextComponent type={type} theme={theme} text={text} color={color} />;
}
