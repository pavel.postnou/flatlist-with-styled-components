import React from "react";
import TextComponent from "../../src/shared/components/Text";
import ImageComponent from "../../src/shared/components/Image";
import {
  User,
  ImgAndName,
  Name,
  ImageBack,
  AllInfo,
} from "./describeScreen.styled";

export default function describeScreen(props) {
  let {
    theme,
    send: { name },
    send: { picture },
    send: { dob },
    send: { gender },
    send: { location },
    send: { email },
    send: { phone },
  } = props.route.params;

  const text = `Hello, i am ${gender}, i live in ${location.street.name} ${
    location.street.number
  } city ${location.city}, state ${location.state}, in ${
    location.country
  }. I am ${dob.age} age old. My date of birth is ${dob.date.slice(
    0,
    10
  )}. I will be glad to talk.`;

  return (
    <User theme={theme}>
      <ImageBack source={theme.image} style={{ flex: 1 }}>
        <ImgAndName theme={theme}>
          <ImageComponent
            type="imgDescription"
            source={{ uri: picture.large }}
          />
          <Name>
            <TextComponent type="describe" theme={theme} text="Name: " />
            <TextComponent type="nameText" theme={theme} text={name.first} />
            <TextComponent type="describe" theme={theme} text="Last Name: " />
            <TextComponent type="nameText" theme={theme} text={name.last} />
            <TextComponent type="describe" theme={theme} text="Age: " />
            <TextComponent type="nameText" theme={theme} text={dob.age} />
          </Name>
        </ImgAndName>
      </ImageBack>
      <ImageBack source={theme.image} style={{ flex: 2 }}>
        <AllInfo theme={theme}>
          <TextComponent
            type="description"
            theme={theme}
            text={text}
          />
          <TextComponent
            type="description"
            color={theme.titleName}
            theme={theme}
            text="email: "
          />
          <TextComponent type="description" theme={theme} text={email} />
          <TextComponent
            type="description"
            color={theme.titleName}
            theme={theme}
            text="phone: "
          />
          <TextComponent type="description" theme={theme} text={phone} />
        </AllInfo>
      </ImageBack>
    </User>
  );
}
